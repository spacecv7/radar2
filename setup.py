# A script to build a .py file from .ui file made in QtCreator.
from setuptools import setup

try:
    from pyqt_distutils.build_ui import build_ui
    cmdclass = {"build_ui": build_ui}
except ImportError:
    cmdclass = {}

setup(
    name="aac",
    version="1.0",
    packages=["aac"],
    cmdclass=cmdclass,
    entry_points={
        'console_scripts': [
            'sapr_aac=aac.__main__:main'
        ]
    }
)
