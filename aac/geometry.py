from typing import Dict, List

import numpy as np

from aac_topology import AACTopology


def topo_to_subarrays(topo: AACTopology) -> List[Dict[str, np.ndarray]]:
    """Конвертация из топологии aac в подрешётки.

    Args:
        topo: топология антенной решётки.

    Returns:
        Список словарей с геометрией подрешёток (в длинах волн).
    """
    # Шаг элементов в длинах волн

    y_step, z_step = topo.X_stepping, topo.Y_stepping

    # Количество элементов в подрешётках
    el_z_count, el_y_count = topo.shape_elements()

    # Количество подрешёток
    sub_z_count, sub_y_count = topo.Rx_submatrix.shape

    # Расположение подрешёток
    sub_matrix = topo.Rx_submatrix

    # Список идентификаторов подрешёток
    sub_ids = np.unique(sub_matrix)

    # Топология элементов
    elements = topo.to_elements(topo.Rx_submatrix)

    # Находим центр (в подрешётках)
    y0 = (el_y_count * sub_y_count - 1) / 2
    z0 = (el_z_count * sub_z_count - 1) / 2

    # Формируем подрешётки
    subarrays = []
    for sub_id in sub_ids[sub_ids > 0]:
        # Координаты антенных решёток от верхнего левого угла
        z, y = np.where(elements == sub_id)

        # Координаты относительно центра
        y, z = y - y0, -(z - z0)
        subarrays.append({
            'elem_x': np.zeros(len(y)),
            'elem_y': y * y_step,
            'elem_z': z * z_step,
            'norm_x': np.zeros(len(y)),
            'norm_y': np.zeros(len(y)),
            'norm_z': np.ones(len(y))
        })
    return subarrays



# def hextopo_to_subarrays(topo: AACTopology) -> List[Dict[str, np.ndarray]]:
#     # Шаг элементов в длинах волн
#
#     y_step, z_step = topo.X_stepping, topo.Y_stepping
#
#     # Количество элементов в подрешётках
#     el_z_count, el_y_count = topo.shape_elements()
#
#     # Количество подрешёток
#     sub_z_count, sub_y_count = topo.Rx_submatrix.shape
#
#     # Расположение подрешёток
#     sub_matrix = topo.Rx_submatrix
#
#     # Список идентификаторов подрешёток
#     sub_ids = np.unique(sub_matrix)
#
#     # Топология элементов
#     elements = topo.to_elements(topo.Rx_submatrix)
#
#
#     #Перевод координат в гексанольаное представление
#     sl = (2 * radius) * math.tan(math.pi / 6)
#     p = sl * 0.5
#     b = sl * math.cos(math.radians(30))
#     w = b * 2
#     h = 2 * sl
#     # offsets for moving along and up rows
#     xoffset = b
#     yoffset = 3 * p
#
#     row = 1
#
#     shifted_xs = []
#     straight_xs = []
#     shifted_ys = []
#     straight_ys = []
#
#     while x < shifted_xs:
#         xs = [startx, startx, startx + b, startx + w, startx + w, startx + b, startx]
#         straight_xs.append(xs)
#         shifted_xs.append([xoffset + x for x in xs])
#         startx += w
#
#     while y_step < el_z_count:
#         ys = [starty + p, starty + (3 * p), starty + h, starty + (3 * p), starty + p, starty, starty + p]
#         (straight_ys if row % 2 else shifted_ys).append(ys)
#         starty += yoffset
#         row += 1
#
#     polygons = [zip(xs, ys) for xs in shifted_xs for ys in shifted_ys] + [zip(xs, ys) for xs in straight_xs for ys in
#                                                                           straight_ys]
#     return polygons


def array_topo_to_subarrays(path: str) -> List[Dict[str, np.ndarray]]:
    # Создаём топологию aac
    topo = AACTopology()
    topo.load(str(path))
    topo.shape()

    # Ковертируем в подрёшётки
    subarrays = topo_to_subarrays(topo)
    return subarrays
