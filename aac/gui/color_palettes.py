# Project: Antenna Array Configuration
# Author: Nikolai Gaiduchenko
# Copyright: Laboratory of Modelling of Special Computer Systems Architectures / MIPT.
# Verion: 1.2

"""Contains classes with color palettes to colorize subarrays.

Two palettes are implemented: `AmericanUI` and `AAP64`. Different palettes can
be implemented in different ways (i.e. `OrderedDict`, lists, etc.), but each of
them have theese similar properties:

-- Each palette class has get_rgb_list(int size) method, that returns a list
   of `(int R, int G, int B)` tuples, generated from the class palette and
   replicated until fits the `size` requested.

Extra color palettes can be implemented later. The colors of the subarrays in
the app can be switched easily be replacing only one string in the `TableModel`
class.
"""
class AmericanUIPalette():
    """American UI Color Palette.

    Colors and their color codes can be found
    on https://flatuicolors.com/palette/us

    Use get_rgb_list(int size) method to get list of (R, G, B) color tuples.
    """

    from collections import OrderedDict
    palette = OrderedDict([
                          ('LightGreenishBlue', ( 85, 239, 196)),
                          ('MintLeaf',          (  0, 184, 148)),
                          ('FadedPoster',       (129, 236, 236)),
                          ('RobinsEggBlue',     (  0, 206, 201)),
                          ('GreenDarnerTail',   (116, 185, 255)),
                          ('ElectronBlue',      (  9, 132, 227)),
                          ('ShyMoment',         (162, 155, 254)),
                          ('ExodusFruit',       (108,  92, 231)),
                          ('SourLemon',         (255, 234, 167)),
                          ('BrightYarrow',      (253, 203, 110)),
                          ('FirstDate',         (250, 177, 160)),
                          ('Orangeville',       (225, 112,  85)),
                          ('PinkGlamour',       (255, 118, 117)),
                          ('ChiGong_modified',  (240, 90 , 90 )),
                          ('Piko8Pink',         (253, 121, 168)),
                          ('PrunusAvium',       (232,  67, 147)),
                          ('CityLights',        (223, 230, 233)),
                          ('SoothingBreeze',    (178, 190, 195)),
                          ('AmericanRiver',     ( 99, 110, 114)),
                          ('DraculaOrchid',     ( 45,  52,  54))
                          ])
    blank = (255, 255, 255)

    def get_rgb_list(self, size=64):
        """
        Returns: A list of (R, G, B) tuples, generated from the class
            palette and replicated until fits the `size:int` requested.
        """
        items = list(self.palette.values())
        rgblist = [self.blank] + items * (size // len(items)) + items[:(size % len(items))]
        return rgblist

class AAP64Palette():
    """AAP-64 Color Palette.

    Colors and their color codes can be found
    on https://lospec.com/palette-list/aap-64

    Use get_rgb_list(int size) method to get list of colors as tuples.
    """
    palette = [(  6,  6,  8), ( 20, 16, 19), ( 59, 23, 37), (115, 23, 45),
               (180, 32, 42), (223, 62, 35), (250,106, 10), (249,163, 27),
               (255,213, 65), (255,252, 64), (214,242,100), (156,219, 67),
               ( 89,193, 53), ( 20,160, 46), ( 26,122, 62), ( 36, 82, 59),
               ( 18, 32, 32), ( 20, 52,100), ( 40, 92,196), ( 36,159,222),
               ( 32,214,199), (166,252,219), (255,255,255), (254,243,192),
               (250,214,184), (245,160,151), (232,106,115), (188, 74,155),
               (121, 58,128), ( 64, 51, 83), ( 36, 34, 52), ( 34, 28, 26),
               ( 50, 43, 40), (113, 65, 59), (187,117, 71), (219,164, 99),
               (244,210,156), (218,224,234), (179,185,209), (139,147,175),
               (109,117,141), ( 74, 84, 98), ( 51, 57, 65), ( 66, 36, 51),
               ( 91, 49, 56), (142, 82, 82), (186,117,106), (233,181,163),
               (227,230,255), (185,191,251), (132,155,228), ( 88,141,190),
               ( 71,125,133), ( 35,103, 78), ( 50,132,100), ( 93,175,141),
               (146,220,186), (205,247,226), (228,210,170), (199,176,139),
               (160,134, 98), (121,103, 85), ( 90, 78, 68), ( 66, 57, 52)]
    blank = (255, 255, 255)

    def get_rgb_list(self, size=64):
        """
        Returns: A list of (R, G, B) tuples, generated from the class
            palette and replicated until fits the `size:int` requested.
        """
        items = self.palette
        rgblist = [self.blank] + items * (size // len(items)) + items[:(size % len(items))]
        return rgblist
