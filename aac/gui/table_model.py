# -*- coding: utf-8 -*-
# Project: Antenna Array Configuration
# Author: Nikolai Gaiduchenko
# Copyright: Laboratory of Modelling of Special Computer Systems Architectures / MIPT.
# Verion: 1.2


"""Setup interaction between AAC topology data and TableView cells.

This module contains classes that re-implement predefined PyQt classes for
QAbstractTableModel. The TableModel class does all the work with the data
representation in QTableView. It also works with color themes of the app.
"""

# Global imports
from PyQt5 import QtCore, QtGui

# Local imports
from . import color_palettes

class TableModel(QtCore.QAbstractTableModel):
    """interact with `AACTopology` data and display it in `TableView`.

    The `TableModel` class does all the work with the data
    representation in `QTableView`. It also works with color themes of the app.

    The `rowCount()` and `columnCount()` methods return the dimensions of the
    table. To retrieve a model index corresponding to an item in the model, use
    `index()` and provide only the row and column numbers.

    Attributes:
        topology (:obj:`AACTopology`): The link to `MainWindow.topology` to
            interact with and get data from.

        DEF_CELL_SIZE (property):
            A [height, width] list for default cell size in pixels. Set to
            [21, 21] during `__init__()`.

        cell_size (:obj:`list`): [y, x] `TableView` cell size (in pixels).
        colors (:obj:`list`): A list with (R, G, B) tuples for subarray colors.
    """

    def __init__(self, topology=None):
        """Initialize `TableModel` attributes.

        Args:
            topology (:obj:`AACTopology`): The link to `MainWindow.topology` to
                interact with and get data from.

        Attributes:
            topology (:obj:`AACTopology`): The link to `MainWindow.topology` to
                interact with and get data from.
            cell_size (:obj:`list`): [y, x] `TableView` cell size (in pixels).
            colors (:obj:`list`): A list with (R, G, B) tuples for subarray
            colors.
        """
        super().__init__()
        if topology is not None:
            self.topology = topology
        else:
            print("TableModel error: can't use NoneType topology.")
            return

        # Get colors for subarrays.
        # Change this line to change the color palette for the entire app.
        self.colors = color_palettes.AmericanUIPalette().get_rgb_list()

        # Create custom role to paint outlining on Tx-selected cells.
        self.TxValueRole = QtCore.Qt.UserRole + 1

        # Init default TableView cell size in pixels
        self.DEF_CELL_SIZE = [21, 21]
        self.cell_size = self.DEF_CELL_SIZE

    # Protect DEF_CELL_SIZE from link comprehension
    DEF_CELL_SIZE = property()

    @DEF_CELL_SIZE.getter
    def DEF_CELL_SIZE(self):
        """Returns a copy of the default cell size.

        Note:
            is a list of [height, width] in pixels.
        """
        return self._DEF_CELL_SIZE.copy()

    @DEF_CELL_SIZE.setter
    def DEF_CELL_SIZE(self, value):
       self._DEF_CELL_SIZE = value

    @DEF_CELL_SIZE.deleter
    def DEF_CELL_SIZE(self):
        self._DEF_CELL_SIZE = None

    def set_cell_size(self, size=None, set_default=False):
        """Set cell size if possible.

        If the set_default option is True, other arguments will be ignored
        and the cell size will return to default.

        Note:
            The minimum cell size is hardcoded to 5x5 pixels.
            The initial default cell size is hardcoded to 21x21 pixels.

        Args:
            size (:obj:`list`): This [y,x] values will be set as new cell size.
            set_default (bool): This option will return cell size to default.
        """
        if set_default:
            self.cell_size = self.DEF_CELL_SIZE
            return

        elif size is None:
            return

        else:
            try:
                if size[0] > 5:
                    self.cell_size[0] = size[0]
                if size[1] > 5:
                    self.cell_size[1] = size[1]

            except IndexError:
                print('ERROR: incorrect cell size assignment.\n'
                      'Setting default cell size.')
                self.set_cell_size(set_default=True)

    def change_cell_size(self, size=None, set_default=False):
        """Change cell size if possible.

        If the set_default option is True, other arguments will be ignored
        and the cell size will return to default.

        Note:
            The minimum cell size is hardcoded to 5x5 pixels.
            The initial default cell size is hardcoded to 21x21 pixels.

        Args:
            size (:obj:`list`): This [y,x] values will be set as new cell size.
            set_default (bool): This option will return cell size to default.
        """
        if set_default:
            self.cell_size = self.DEF_CELL_SIZE
            return

        elif size is None:
            return

        else:
            try:
                if self.cell_size[0] + size[0] > 5:
                    self.cell_size[0] += size[0]
                if self.cell_size[1] + size[1] > 5:
                    self.cell_size[1] += size[1]

            except IndexError:
                print('ERROR: incorrect cell size assignment.\n'
                      'Setting default cell size.')
                self.set_cell_size(set_default=True)

    def rowCount(self, parent=QtCore.QModelIndex()):
        """Return the number of rows of the Rx_data table."""
        return self.topology.Rx_data.shape[0]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """Return the number of columns of the Rx_data table."""
        return self.topology.Rx_data.shape[1]

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """Setup table header behaviour."""
        if role == QtCore.Qt.DisplayRole:
            return section
        elif role == QtCore.Qt.SizeHintRole:
            return QtCore.QSize(*self.cell_size)
        else:
            return QtCore.QVariant()

    def size(self):
        """Return the size of the whole table as :obj:`QSize`."""
        return QtCore.QSize((self.rowCount()+1)*self.cell_size[0],
                            (self.columnCount()+1)*self.cell_size[1])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """Returns the data stored under the given role for the item.

        Returns the `data` stored under the given `role` for the item referred
        to by the `index`.

        For `DisplayRole`: returns a QString with the subarray number assigned
        to the cell and extracted from `topology.Rx_data`. Fault cells contain
        `-1` in the `topology.Rx_data`, but represented as 'X' characters.
        Empty cells are just empty. Note that if the cell size is too small to
        display the text, an empty string will be returned.

        For `TextAlignmentRole`: returns horizontally and vertically alignment.

        For `BackgroundRole`: returns a :obj:`QBrush` with the subarray color
            to paint the background of the cell.

        For `ForegroundRole`: computes the font color based on the cell
            background color and returns a :obj:`QBrush` with the font color.
            The best solution to the font color on differently colored
            background problem is to select white font for "dark" colors, and
            black font for "bright" colors.

            See also: `TableModel.get_contrast_font_color()`

        For `TxValueRole`: This is a custom role, for which TableModel returns
            `1` if the cell must be outligned as `Tx-selected` and `0`
            otherwise.

            See also: `TableViewDelegate` documentation for `TxValueRole`.

        Note:
            If you do not have a value to return, returns a QVariant instead
            of returning 0.

        Args:
            index (:obj:`QModelIndex`): The index of the table cell.
            role (:obj:`ItemDataRole`): The role used by the view to indicate
                to the model which type of data it needs.
 """

        if not index.isValid():
            return QtCore.QVariant()

        i = index.row()
        j = index.column()

        if role == QtCore.Qt.DisplayRole:
            # The key data to be rendered in the form of text. (QString)
            if min(self.cell_size) < 10:              # small
                return ''
            elif self.topology.Rx_data[i, j] == 0:    # empty
                return ''
            elif self.topology.Rx_data[i, j] == -1:   # fault
                return 'X'
            else:
                return '{0}'.format(self.topology.Rx_data[i, j]) # normal

        if role == QtCore.Qt.TextAlignmentRole:
            # We want center text alignment
            return QtCore.Qt.AlignCenter

        if role == QtCore.Qt.BackgroundRole:
            # The background brush used for items rendered with the default
            # delegate. (QBrush)
            return QtGui.QBrush(self.get_qt_color(self.topology.Rx_data[i, j]))

        if role == QtCore.Qt.ForegroundRole:
            # The foreground brush (text color, typically) used for items
            # rendered with the default delegate. (QBrush)
            font_color = self.get_contrast_font_color(self.topology.Rx_data[i, j])
            return QtGui.QBrush(font_color)


        if role == self.TxValueRole:
            return self.topology.Tx_data[i, j]

        return QtCore.QVariant()

    def update_index(self, row, col):
        """Force-repaint certain `TableView` cell item.

        Args:
            row, col (int, int): coordinates of the cell needed repaint.
        """
        self.dataChanged.emit(self.index(row, col), self.index(row, col))

    def flags(self, index):
        """Is the `TableView` item enabled or not?"""
        return QtCore.Qt.ItemIsEnabled

    def get_qt_color(self, subarray_number=1):
        """Returns :obj:`QColor` for the `subarray_number` (int) given."""

        color = QtGui.QColor()
        if subarray_number == 0 or subarray_number == -1:
            # Empty and fault cells have white colors
            color.setRgb(255, 255, 255)
        else:
            color.setRgb(*self.colors[subarray_number])
        return color

    def get_contrast_font_color(self, data=0):
        """Get the font color based on cell's background color.

        The best solution to the font color on differently colored background
        problem is to select white font for "dark" colors, and black font for
        "bright" colors. This is done by counting the "perceptive luminance" for
        the subarray color from the palette. The formula is:

            luminance = 1 - (Rc * R + Gc * G + Bc * B) / 255

        where Rc, Gc, Bc are RGB perceptive coefficients and the division by 255
        is made for normalisation. The algorithm takes a weighted sum of the RGB
        components of the color and counts it's luminance. If it is lower than
        0.5, then the color given is bright, so we must return black font color.

        Args:
            data (int): the subarray number.

        Returns:
            white or black QColor object to set the font color.
        """
        # Get the background color of the cell
        color = self.get_qt_color(data)

        font_color = 0                       # future font color
        # Counting the perceptive luminance - human eye favors green color...
        luminance = 1 - (0.299*color.red()   +
                         0.587*color.green() +
                         0.114*color.blue()) / 255

        if luminance < 0.5:
            font_color = 0   # bright colors - black font
        else:
            font_color = 255 # dark colors - white font

        return QtGui.QColor(font_color, font_color, font_color)
