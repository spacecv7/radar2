# -*- coding: utf-8 -*-
# Project: Antenna Array Configuration
# Author: Nikolai Gaiduchenko
# Copyright: Laboratory of Modelling of Special Computer Systems Architectures / MIPT.
# Verion: 1.2

"""Re-implement `TableView` classes and methods.

This module contains classes that re-implement predefined PyQt classes for
`QTableView`.
"""

from PyQt5 import QtCore, QtWidgets

class TableViewDelegate(QtWidgets.QStyledItemDelegate):
    """`QTableView` cell item delegate.

    Re-implements the paint method to draw custom objects in `QTableView` cells
    based on the role.

    For `BackgroundRole` in `TableModel` you must always return `QBrush`. You
    can use `QBrush.setTexture(:obj:`QPixmap`)` to set the image use to draw
    your background. But then it must be a correct size, so it is not drawn
    couple of times.

    The only reasonable solution would be to subclass `QStyledItemDelagate`
    (not `QItemDelegate`) and reimplement paint method.

    Use `setItemDelegate` method to set a delegate on your `TableView`.

    `TxValueRole`:

        The `TxValueRole` is a custom role that is created in `TableModel` class
        and then passed to `QTableView`. The `QTableView` paint method is
        re-implemented, so the custom role is catched and handled.

        The role of `TxValueRole` is to carry the `Tx_data` value for the cell
        that is being painted. If the `Tx_data` conttains non-zero value, then
        the cell is selected for transmition and the outlining must be drawn.
    """

    def __init__(self, parent=None):
        """Initialize as default and then create `TxValueRole`."""
        super().__init__(parent)
        self.TxValueRole = QtCore.Qt.UserRole + 1
        self.colors = [QtCore.Qt.red, QtCore.Qt.red, QtCore.Qt.black]

    def paint(self, painter, option, index):
        """Paint cell decorative elements, such as outlining for selected cells.

        The method saves the painter and draws all the neccessary elements,
        then restores the painter.

        You can draw as many rectangle outlinings (one inside another) as you
        want simply just by adding `Qt.Colors` to the self.colors list that was
        initialised in `__init__()` method.

        In `TableModel` class `Tx_data` object contains information about all
        the cells. If the cell is selected for transmition, then

            Tx_data[cell] == 1

        The `Tx_data[cell]` data is brought into this method using a custom
        `TxValueRole`. So, if the `TxValueRole` data is non-zero, the outlining
        must be drawnself.

        The outlining is drawn as multiple rectangles, one inside another. If
        colors list was not specified, the default color sequence is `2 px` red
        and `1 px` black outline, one inside another.
        """

        # Paint as default
        QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)

        # If the cell is selected for transmition,
        # the outlining must be drawn.
        if index.data(self.TxValueRole) == 1:
            painter.save()

            # draw outlining around cell
            pen = painter.pen()

            # You can draw as many rectangle outlinings (one inside another)
            # as you want simply just by adding colors to the 'colors' list.

            for i, color in enumerate(self.colors):
                pen.setColor(color)
                painter.setPen(pen)
                rect = QtCore.QRect(option.rect)
                rect.adjust(i, i, -pen.width()-i, -pen.width()-i)
                painter.drawRect(rect)

            painter.restore()
