# Project: Antenna Array Configuration
# Author: Nikolai Gaiduchenko
# Copyright: Laboratory of Modelling of Special Computer Systems Architectures / MIPT.
# Verion: 1.2

"""The topology classes of the Antenna subArray Configuration."""

import datetime
import os

import numpy as np


class AACTopology:
    """Antenna subArray Configuration topology class.

    ## A description of the format of the antenna configuration file:

    The file with the antenna array configuration data is saved in text form
    with the extension TXT.

    General structure of the file:

        line 1:
        line 2:     commentary line (version)
        line 3:     commentary line (date)
        line 4:
        1st matrix: NY lines of the configuration matrix of the receiving
                    modules.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 of the configuration of the
                    transmitting modules (optional)

        (where NY is the matrix height (the number of elements in the column))

    ## Detailed file format:
        8 numbers in the first line:
        - grid width (number of antenna elements per row)
        - grid height (number of antenna elements per column)
        - module width (in antenna elements)
        - the height of the module (antenna elements)
        - Tx antenna X origin (=-1 if the configuration matrix of the
            receiving antenna is specified in the file)
        - TX antenna Y origin (=-1 if the configuration matrix of the
            receiving antenna is specified in the file)
        - width of the transmitting antenna array (in elements)
        - the height of the transmitting antenna array (in elements)

        8 numbers in the second line :
        - x step between antenna elements (relative to wavelength)
        - m step between antenna elements (relative to wavelength)
        - flag to use triangular mesh for the antenna elements
        - window function usage flag (1, 2 or 3)
        - gain (gain of the antenna element in DB)
        - gain error (antenna element gain error in DB)
        - phase error (STD phase error of the antenna element in degrees)

    ### Each antenna element of the receiving array configuration matrix means:
        - if >0 - the subarray number to which the antenna element is assigned.
        - if =0 - empty antenna element.
        – if <0 - fault, defective item.

    ### Each antenna element of the configuration matrix of the transmitting
    lattice means:
        - if >0 - this antenna element is included in the transmitting grid.
        - if =0 - this antenna element is not included in the transmission grid.

    Note:
        You can see some examples of topology in the `aac/data` folder.

    Attributes:
        # Global constants
        version (float): topology version
        subarrays (int): number of subarrays available

        # Topology default data matrices
        def_shapes : [16, 16]
        Rx_data : np.zeros(self.def_shapes, dtype:int)
        Tx_data : np.zeros(self.def_shapes, dtype:int)
        Rx_submatrix : None
        Tx_submatrix : None

        # Default file paths
        dirpath (str):
            Default: './aac/data'
            Default path to the data directory with AAConfigs

        fname_def_save (str):
            Default: 'saved.txt'
            Default filename to save created AAC to.

        fname_def_recent (str):
            Default: 'recentconfig.txt'
            Default filename for loading from recently closed AAC.

        fname_def_default (str):
            Default: 'defconfig.txt'
            Default filename to load AAC from when the `Default` button
            is pressed.

        fpath_save (str):
            Default: './aac/data/saved.txt'
            Default file path to save created AAC to.

        fpath_recent (str):
            Default: './aac/data/recentconfig.txt'
            Default file path for loading from recently closed AAC.

        fpath_default (str):
            Default: './aac/data/defconfig.txt'
            Default file path to load AAC from when the `Default` button is
            pressed.

        fpath_loaded (str):
            Default: ''
            File path the AAC topology was loaded from.

        # Topology attributes:
        Rx_grid_width_elem (int):       Rx grid width (in elements)
        Rx_grid_height_elem (int):      Rx grid height (in elements)
        Tx_grid_width_elem (int):       Tx grid width (in elements)
        Tx_grid_height_elem (int):      Tx grid height (in elements)
        module_width_elem (int):        Module width (in elements)
        module_height_elem (int):       Module height (in elements)
        Tx_X_origin (int):              Tx antenna X origin (-1 if the config
                                        matrix is for the receiver)
        Tx_Y_origin (int):              Tx antenna X origin (-1 if the config
                                        matrix is for the receiver)
        X_stepping (float):             X stepping in lambda
        Y_stepping (float):             Y stepping in lambda
        triangle_grid_flag (1/0):       triangle grid flag
        taylor_wnd_function_flag (1/0): taylor WND function flag
        antenna_element_type (int):     antenna element type
        gain_dB (float):                gain in dB
        gain_error (float):             gain STD error
        phase_error (float):            phase STD error
        Nelements (int):                number of antenna elements
        Nmodules (int):                 number of antenna modules
        Nsubarrays (int):               number of subarrays
        Tx_cells (int):                 number of transmitter marked cells
    """

    def __init__(self):
        """Init empty topology."""
        # Init topology constants
        self.version = 1.0
        self.subarrays = 0

        # Init topology default data matrices
        self.def_shapes = [16, 16]
        self.Rx_data = np.zeros(self.def_shapes, dtype=int)
        self.Tx_data = np.zeros(self.def_shapes, dtype=int)
        self.Rx_submatrix = None
        self.Tx_submatrix = None

        # Set default file paths
        self.dirpath = os.path.join(os.path.dirname(__file__), 'data')
        self.fname_def_save = 'saved.txt'
        self.fname_def_recent = 'recentconfig.txt'
        self.fname_def_default = 'defconfig.txt'
        self.fpath_save = os.path.join(self.dirpath, self.fname_def_save)
        self.fpath_recent = os.path.join(self.dirpath, self.fname_def_recent)
        self.fpath_default = os.path.join(self.dirpath, self.fname_def_default)
        self.fpath_loaded = ''

        # Init default topology variables
        self.Rx_grid_width_elem = 0         # Rx grid width (elements)
        self.Rx_grid_height_elem = 0        # Rx grid height (elements)
        self.Tx_grid_width_elem = 0         # Tx grid width (elements)
        self.Tx_grid_height_elem = 0        # Tx grid height (elements)
        self.module_width_elem = 0          # Module width (elements)
        self.module_height_elem = 0         # Module height (elements)
        self.Tx_X_origin = -1               # Tx antenna X origin
        self.Tx_Y_origin = -1               # Tx antenna X origin
        self.X_stepping = 0.5               # X stepping in lambda
        self.Y_stepping = 0.5               # Y stepping in lambda
        self.triangle_grid_flag = 0         # triangle grid flag
        self.taylor_wnd_function_flag = 0   # taylor WND function flag
        self.antenna_element_type = 0       # antenna element type
        self.gain_dB = 0                    # gain in dB
        self.gain_error = 0                 # gain STD error
        self.phase_error = 0                # phase STD error
        self.Nelements = 0                  # number of antenna elements
        self.Nmodules = 0                   # number of antenna modules
        self.Nsubarrays = 0                 # number of subarrays
        self.Tx_cells = 0                   # number of transmitter marked cells

    def __repr__(self):
        """Conviniently print topology information in terminal."""
        return """AAC Topology  {}
              Version: {}
              Module width   (elements): {}
              Module height  (elements): {}
              Rx grid width  (elements): {}
              Rx grid height (elements): {}
              Tx grid width  (elements): {}
              Tx grid height (elements): {}
              X stepping in lambda     : {}
              Y stepping in lambda     : {}
              Triangle grid flag       : {}
              Taylor wnd function flag : {}
              Antenna element type     : {}
              Gain (dB value)          : {}
              STD gain error           : {}
              STD phase error          : {}
              Number of elements       : {}
              Number of modules        : {}
              Number of subarrays      : {}
              Number of Tx cells       : {}
              """.format(self.fpath_loaded,
                         self.version,
                         self.module_width_elem,
                         self.module_height_elem,
                         self.Rx_grid_width_elem,
                         self.Rx_grid_height_elem,
                         self.Tx_grid_width_elem,
                         self.Tx_grid_height_elem,
                         self.X_stepping,
                         self.Y_stepping,
                         'checked' if self.triangle_grid_flag else 'unchecked',
                         'checked' if self.taylor_wnd_function_flag else 'unchecked',
                         self.antenna_element_type,
                         self.gain_dB,
                         self.gain_error,
                         self.phase_error,
                         self.Nelements,
                         self.Nmodules,
                         self.Nsubarrays,
                         self.Tx_cells
                        )

    def set_Rx_data(self, data=None, row=None, col=None):
        """Set the `Rx_data[row, col]` or replace `Rx_data` entirely.

        Note:
            If `data` argument is set to `None`, the `topology.Rx_data` will be
            set as empty table with `AACTopology.def_shapes` default shapes.

            If `data` is int and `row`, `col` are not `None`, then `data` will
            be interpreted as a new value to assign to `Rx_data[row, col]`.

            Otherwise, if `row` and `col` are `None`, the data will be
            interpreted as two-dimensional `np.array` and `AACTopology.Rx_data`
            will be replaced by the `data` given entirely.

        Args:
            data (int, :obj:`np.array`): data to assign.
            row, col (int, int): optional coords to specify data assignment.
        """
        if data is None:
            self.Rx_data = np.zeros(self.def_shapes, dtype=int)
        elif row is not None and col is not None:
            self.Rx_data[row, col] = data
        else:
            self.Rx_data = data

    def set_Tx_data(self, data=None, row=None, col=None):
        """Set the `Tx_data[row, col]` or replace `Tx_data` entirely.

        Note:
            If `data` argument is set to `None`, the `topology.Tx_data` will be
            set as empty table with `AACTopology.def_shapes` default shapes.

            If `data` is int and `row`, `col` are not `None`, then `data` will
            be interpreted as a new value to assign to `Tx_data[row, col]`.

            Otherwise, if `row` and `col` are `None`, the data will be
            interpreted as two-dimensional `np.array` and `AACTopology.Tx_data`
            will be replaced by the `data` given entirely.

        Args:
            data (int, :obj:`np.array`): data to assign.
            row, col (int, int): optional coords to specify data assignment.
        """
        if data is None:
            self.Tx_data = np.zeros(self.def_shapes, dtype=int)
        elif row is not None and col is not None:
            self.Tx_data[row, col] = data
        else:
            self.Tx_data = data

    def shape_elements(self):
        """Returns: The shape of the single module."""
        return [self.module_height_elem, self.module_width_elem]

    def submatrix(self, arr=None):
        """Returns: the non-zero submatrix of the `arr` - given `np.array`."""
        if not arr.any():
            return np.zeros(self.def_shapes, dtype=int)

        x, y = np.nonzero(arr)
        # Using the smallest and largest x and y indices of nonzero elements,
        # we can find the desired rectangular bounds.
        # And don't forget to add 1 to the top bound to avoid the
        # fencepost problem.
        if not arr.any():
            return None
        else:
            return arr[x.min():x.max()+1, y.min():y.max()+1]

    def shape(self):
        """Returns the shape of `Rx_submatrix`, when `Rx_submatrix` is computed.

        `Tx_submatrix` is computed as the `Tx_data` slice with shapes from
        `Rx_submatrix` to match the data between two submatrices.
        """
        arr_Rx, arr_Tx = self.Rx_data, self.Tx_data

        # If arr contains only zeros, return immediately
        if not arr_Rx.any():
            self.Rx_submatrix = np.zeros(self.def_shapes, dtype=int)
            self.Tx_submatrix = np.zeros(self.def_shapes, dtype=int)
            return self.def_shapes

        x, y = np.nonzero(arr_Rx)

        # Using the smallest and largest x and y indices of nonzero elements,
        # we can find the desired rectangular bounds. And don't forget to add
        # 1 to the top bound to avoid the fencepost problem.
        arr_Rx = arr_Rx[x.min():x.max()+1, y.min():y.max()+1]

        if not arr_Tx.any():
            arr_Tx = np.zeros_like(arr_Rx, dtype=int)
        else:
            arr_Tx = arr_Tx[x.min():x.max()+1, y.min():y.max()+1]

        self.Rx_submatrix, self.Tx_submatrix = arr_Rx, arr_Tx
        return self.Rx_submatrix.shape

    def to_elements(self, data: object = None, shapes: object = None) -> object:
        """
        Convert the `data` given to elements representation
        with the `shapes` given.
        """
        if data is None:
            data = self.Rx_submatrix

        if shapes is None:
            shapes = self.shape_elements()

        data_elem = np.repeat(np.repeat(data, shapes[0], axis=0),
                              shapes[1], axis=1)
        return data_elem

    def to_modules(self, data=None, shapes=None):
        """
        Convert the `data` given to module representation
        with the `shapes` given.
        """

        if data is None:
            return self.Rx_submatrix

        if shapes is None:
            shapes = self.shape_elements()

        data_mod = data[::shapes[0], ::shapes[1]]
        return data_mod

    def check_fpaths_correct(self):
        """Check if defined in topoology filepaths exist and return `bool`."""
        if not os.path.exists(self.fpath_recent) or \
           not os.path.exists(self.fpath_default) or \
           not os.path.exists(self.fpath_save):
            return False
        else:
            return True

    def save(self, fpath):
        """Save topology to the `fpath` given.

        Note:
            If there is no fpath, default fpath is chosen.
        """
        # Handle incorrect path errors
        if fpath == '' or fpath is None:
            if not self.check_fpaths_correct():
                return
            else:
                fpath = self.fname_def_save

        # Get current date
        now = datetime.datetime.now()
        now_str = now.strftime("%d-%m-%Y %H:%M:%S")

        # Save AAC to file
        with open(fpath, 'w') as file_save:
            # Write header comments
            file_save.write('%\n% Version {}\n% Date: {}\n%\n'.format(self.version, now_str))

            # Write head
            file_save.write('{} {} {} {} {} {} {} {}\n'.format(
                self.Rx_grid_width_elem,
                self.Rx_grid_height_elem,
                self.module_width_elem,
                self.module_height_elem,
                self.Tx_X_origin, # Tx antenna X origin
                self.Tx_Y_origin, # Tx antenna Y origin
                self.Tx_grid_width_elem,
                self.Tx_grid_height_elem))

            file_save.write('{:.3f} {:.3f} {} {} {} {:.3f} {:.3f} {:.3f}\n'.format(
                self.X_stepping,                 # X stepping in lambda
                self.Y_stepping,                 # Y stepping in lambda
                self.triangle_grid_flag,         # triangle grid flag
                self.taylor_wnd_function_flag,   # taylor WND func
                self.antenna_element_type,       # antenna element type
                self.gain_dB,                    # gain in dB
                self.gain_error,                 # gain STD error
                self.phase_error))               # phase STD error

        # Reopen file_save to write bytes
        with open(fpath, 'ab') as file_save_b:
            # Write Rx matrix body, stretched to be in elements
            # instead of modules
            np.savetxt(file_save_b,
                       self.to_elements(self.Rx_submatrix),
                       fmt="%d")

            # Write Tx matrix body (with heading comment),
            # stretched to be in elements instead of modules
            Tx_header = '\n Tx array configuration matrix'
            np.savetxt(file_save_b,
                       self.to_elements(self.Tx_submatrix),
                       fmt='%d',
                       header=Tx_header,
                       comments='%')

    def load(self, fpath):
        """Load topology from the `fpath` given.

        Note:
            If there is no fpath, `recentconfig` fpath is chosen.
        """
        # Handle incorrect path errors
        if fpath == '' or fpath is None:
            if not self.check_fpaths_correct():
                return
            else:
                fpath = self.fpath_recent

        # Save fpath from which loaded for some purpose
        self.fpath_loaded = fpath

        try:
            with open(fpath, 'r') as file_load:

                # PARSE 1st HEADER
                head_comments = 0
                line = file_load.readline()
                while line == '' or line[0] == '%':
                    head_comments += 1
                    line = file_load.readline()
                header1 = list(map(int, line.split()))

                self.Rx_grid_width_elem = header1[0]
                self.Rx_grid_height_elem = header1[1]
                self.module_width_elem = header1[2]
                self.module_height_elem = header1[3]
                self.Tx_X_origin = header1[4]
                self.Tx_Y_origin = header1[5]


                # PARSE 2nd HEADER
                line = file_load.readline()
                while line == '' or line[0] == '%':
                    head_comments += 1
                    line = file_load.readline()
                header2 = line.split()

                self.X_stepping = float(header2[0])
                self.Y_stepping = float(header2[1])
                self.triangle_grid_flag = int(header2[2])
                self.taylor_wnd_function_flag = int(header2[3])
                self.antenna_element_type = int(header2[4])
                self.gain_dB = float(header2[5])
                self.gain_error = float(header2[6])
                self.phase_error = float(header2[7])

        except FileNotFoundError:
            # Handle fpath doesn't exist error and load empty table
            self.__init__()
            return

        # Read all the data
        load_data = np.genfromtxt(fpath,
                                  dtype=int,
                                  comments='%',
                                  skip_header=head_comments+2,
                                  delimiter=' '
                                 )

        try:

            # If Tx matrix is described in the configuration file
            if self.Tx_X_origin == -1 and self.Tx_Y_origin == -1:
                # Split the data into Rx and Tx data.
                split_data = np.split(load_data, 2)
                # Resize data to be written in modules (table cells) instead
                # of elements
                self.set_Rx_data(self.to_modules(split_data[0]))
                self.set_Tx_data(self.to_modules(split_data[1]))
                return

            else:
                # Load Rx as default, Tx is created with same shapes as Rx.
                self.set_Rx_data(self.to_modules(load_data))
                self.set_Tx_data(np.zeros(self.Rx_data.shape, dtype=int))

        except IndexError:
            # Handle fpath doesn't exist error and load empty table
            print('An error occured while parsing the', fpath, 'file.\n\
                   The empty table will be generated instead.')
            self.set_Rx_data(None)
            self.set_Tx_data(None)
            return
