from typing import Dict, List

import numpy as np

from aac.aac_topology import AACTopology


def topo_to_subarrays(topo: AACTopology) -> List[Dict[str, np.ndarray]]:
    """Конвертация из топологии aac в подрешётки.

    Args:
        topo: топология антенной решётки.

    Returns:
        Список словарей с геометрией подрешёток (в длинах волн).
    """
    # Шаг элементов в длинах волн

    y_step, z_step = topo.X_stepping, topo.Y_stepping

    # Количество элементов в подрешётках
    el_z_count, el_y_count = topo.shape_elements()

    # Количество подрешёток
    sub_z_count, sub_y_count = topo.Rx_submatrix.shape

    # Расположение подрешёток
    sub_matrix = topo.Rx_submatrix

    # Список идентификаторов подрешёток
    sub_ids = np.unique(sub_matrix)

    # Топология элементов
    elements = topo.to_elements(topo.Rx_submatrix)

    # Находим центр (в подрешётках)
    y0 = (el_y_count * sub_y_count - 1) / 2
    z0 = (el_z_count * sub_z_count - 1) / 2

    # Формируем подрешётки
    subarrays = []
    for sub_id in sub_ids[sub_ids > 0]:
        # Координаты антенных решёток от верхнего левого угла
        z, y = np.where(elements == sub_id)

        # Координаты относительно центра
        y, z = y - y0, -(z - z0)
        subarrays.append({
            'elem_x': np.zeros(len(y)),
            'elem_y': y * y_step,
            'elem_z': z * z_step,
            'norm_x': np.zeros(len(y)),
            'norm_y': np.zeros(len(y)),
            'norm_z': np.ones(len(y))
        })
    return subarrays


def array_topo_to_subarrays(path: str) -> List[Dict[str, np.ndarray]]:
    # Создаём топологию aac
    topo = AACTopology()
    topo.load(str(path))
    topo.shape()

    # Ковертируем в подрёшётки
    subarrays = topo_to_subarrays(topo)
    return subarrays
