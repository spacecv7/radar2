.PHONY: remake

PACKAGENAME = aac
LOG = ./aac/log/pylint

all: ui

ui:
	python3 setup.py build_ui

run:
	python3 -m $(PACKAGENAME)


remake: all run

pylint: clean_log
	touch $(LOG)
	pylint aac > $(LOG)
	vim $(LOG)

clean_log:
	rm -f $(LOG)
